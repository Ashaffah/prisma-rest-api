import express from "express";
import createError from "http-errors";
import http from "http";
import cors from "cors";
import cookieParser from "cookie-parser";
import dotenv from "dotenv";
import userRoute from "./routes/userRouter.js";
import postRoute from "./routes/postRouter.js";

dotenv.config();
const app = express();
app.use(express.json());

app.use(cors({ origin: `http://localhost:3000` }));
app.use(cookieParser());
app.get("/", (req, res) => {
  res.send(`
  <div style="text-align:center">
    <div style="font-size: 70px">WELCOME</div>
    <div style="font-size: 40px">TO BACKEND SERVICE</div>
  </div>
  `);
});

// API Group Routes
app.use("/tweet", postRoute);
app.use("/user", userRoute);
// handle 404 error
app.use((req, res, next) => {
  next(createError(404));
});

const server = http.createServer(app);

server.listen(process.env.PORT, () => {
  console.log(`⚡️[server]: Server is running at *:${process.env.PORT}`);
});
