import db from "../config/config.js";

export const createUser = async (req, res) => {
  try {
    await db.user.create({
      data: { ...req.body },
    });
    res.json({ message: "User Created!" });
  } catch (error) {
    if (error instanceof Error) {
      res.json({ message: error.message });
    } else {
      res.status(500).json({ message: "Internal server error" });
    }
  }
};

export const getUserByUsername = async (req, res) => {
  try {
    const { username } = req.params;
    const user = await db.user.findUnique({
      where: { username: String(username) },
    });
    res.json({ data: user, message: `Success get ${username} data!` });
  } catch (error) {
    if (error instanceof Error) {
      res.json({ message: error.message });
    } else {
      res.status(500).json({ message: "Internal server error" });
    }
  }
};
