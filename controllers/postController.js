import db from "../config/config.js";

// Create Data
export const createPost = async (req, res) => {
  try {
    const { content, authorEmail } = req.body;
    await db.post.create({
      data: {
        content,
        author: { connect: { email: authorEmail } },
      },
    });
    res.json({ message: "Data Created!" });
  } catch (error) {
    if (error instanceof Error) {
      res.json({ message: "authorEmail tidak ada!" });
    } else {
      res.status(500).json({ message: "Internal server error" });
    }
  }
};
