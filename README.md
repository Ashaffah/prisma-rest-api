# prisma-rest-api

## To Do

- [x] Implement Post Data
- [ ] Implement Get All Data (implement pagination and filtering is a plus)
- [ ] Implement Get Data by id
- [ ] Implement Update Data by id
- [ ] Implement Soft Delete Data by id
- [ ] Implement Hard Delete Data by id
- [ ] Implement Get All Soft Deleted Data (implement pagination and filtering is a plus)
- [ ] Implement Get Soft Deleted Data by id
- [ ] Implement Restore Deleted Data by id
- [ ] Implement Restore All Deleted Data

- [Clue Study](https://www.prisma.io/docs)

## Getting Started

This project uses NPM or YARN as package manager.

### Requirements

Node Version^18.16.0 or higher.
If you haven't installed node js, please visit the link below:

https://nodejs.dev/en/download

NPM Version^8.11.0 or higher.

```bash
npm install -g npm
```

YARN tbd, because i'm not using yarn. But if you prefer use yarn follow this command:

```bash
npm install --global yarn
```

### Install

Using NPM

```bash
npm install
#or
npm i
```

Using YARN

```bash
yarn install
```

Using Bun

```bash
bun install
```

### Usage

Create MySQL db with name study

DB Migrate

```bash
npx prisma migrate dev
```

Use this command to start the project

```bash
# Running the environment
# using NPM
npm run start
# Using Bun
bun run start
```

### Used technologies

[Express.js](https://expressjs.com/en/starter/installing.html)

[Prisma ORM](https://www.prisma.io/docs)

## License

MIT
